#include "Sales.h"


Sales::Sales()
{
}

double Sales::CalculateSalary(std::string date)
{
	//0.003 �� �������� �����������
	//��������� �������� ����������� ���� ������� ������� ���� ��������
	double result = 0;
	//1 ������� �� ������ ��� ������
	const double procentForEveryYears = 0.01;
	//����������� ���������� ������� �� ����
	double additionalSalaryForYears = 0.35;
	for each (Collaborator* col in _vecSubordinates)
	{
		result+=col->CalculateSalary(date);
	}

	int _yearsOfWorking = CountYearsOfWorkign(date);
	//���� ����� ������ ������ ��� �� ������������ �������, ������������� ���� �������
	if (_yearsOfWorking * procentForEveryYears < additionalSalaryForYears) 
	{
		additionalSalaryForYears = _yearsOfWorking * procentForEveryYears;
	}
	_salary = _baseRate + _baseRate * additionalSalaryForYears +result*0.003; 
	result+=_salary;
	return result;
}
Sales::~Sales()
{
}
std::string Sales::getTypeName()
{
	return _SALES;
}