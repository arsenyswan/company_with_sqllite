#include "Manager.h"


Manager::Manager()
{
}

double Manager::CalculateSalary(std::string date)
{
	//0.005 �� �������� ����������� ������� ������
	//��������� �������� ����������� ���� ������� ������� ���� ��������
	double result = 0;
	//��������� �������� ����������� ������� ������
	double sumSallarySubordinates = 0;
	//5 ��������� �� ������ ��� ������
	const double procentForEveryYears = 0.05;
	//����������� ���������� ������� �� ����
	double additionalSalaryForYears = 0.4;
	for each (Collaborator* col in _vecSubordinates)
	{
		result+=col->CalculateSalary(date);
		sumSallarySubordinates += col->getSalary();	
	}
	int _yearsOfWorking = CountYearsOfWorkign(date);
	if (_yearsOfWorking * procentForEveryYears < additionalSalaryForYears) 
	{
		additionalSalaryForYears = _yearsOfWorking * procentForEveryYears;
	}
	_salary = _baseRate + _baseRate*additionalSalaryForYears + sumSallarySubordinates*0.005;
	result+=_salary;
	return result;
}
Manager::~Manager()
{
}
std::string Manager::getTypeName()
{
	return _MANAGER;
}
