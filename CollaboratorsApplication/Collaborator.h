#pragma once
#include <vector>
#include <ctime>

#include <iomanip>
#include <iostream>
#include <string>

#define _MANAGER "MANAGER"
#define _SALES "SALES"
#define _EMPLOYEE "EMPLOYEE"
#define _WITHOUTBOSS -1
//const int SECONDS_IN_YEAR = 31536000;//���������� ������ � ����
class Collaborator //����������� ����� ����������
{
public:
	Collaborator();
	~Collaborator();
	virtual double CalculateSalary(std::string date) = 0;
	void setBaseRate(std::string baseRate){ _baseRate = atof(baseRate.c_str()); };
	void setDayOfHiring(std::string dayOfHiring) 
	{
		//struct std::tm tm;
		//std::istringstream ss("16:35:12");
		//ss >> std::get_time(&tm, "%H:%M:%S"); // or just %T in this case
		//std::time_t time = mktime(&tm);
		while (dayOfHiring.length() < 9)
		{
			std::cout << "Incorrect date, try again" << std::endl;
			std::cin >> dayOfHiring;
		}
		
		{
			_dayOfHiring = dayOfHiring;
		}
	};
	void printSubordinates(std::ostream &of);
	void setNameCollaborator(std::string nameCollaborator) { _nameCollaborator = nameCollaborator; };
	void setID (std::string ID) {_ID = atoi(ID.c_str());}
	void setBossID(std::string bossID) { _bossID = atoi(bossID.c_str());}
	virtual std::string getTypeName() = 0;

	static std::string SqlCommandToCreateTableInDB(std::string nameOfTable);
	static std::string SqlCommandToCreateTableInDB();
	static std::string Collaborator::sqlCommandToInsertDataInTable(std::string nameOfTable, std::string type, std::string name, std::string dayOfHiring, std::string baseRate, std::string bossID);
	static std::string Collaborator::sqlCommandToInsertDataInTable(std::string nameOfTable, std::string type, std::string name, std::string dayOfHiring, std::string baseRate);
	
	static std::string Collaborator::sqlCommandToInsertDataInTable(std::string nameOfTable, Collaborator *col);
	static std::string Collaborator::sqlCommandToUpdateDataInTable(std::string nameOfTable, Collaborator *col, int ID);
	static std::string Collaborator::SqlCommandToCreatDependencyTableInDB();
	
	void addSubordinate(Collaborator* subordinate);
	void setBoss(Collaborator* boss);
	static void fillSubordinatesList(std::vector<Collaborator*> & vec);
	void print(std::ostream &of);
	void print();
	void addCollaboratorToVector(std::vector<Collaborator*> * vec, char **argv);

	int getBossID();
	std::string getName();
	std::string getBaseRate();
	std::string getDayOfHiring();
	double getSalary();
protected:
	int _ID;
	int _bossID;
	Collaborator *_boss;
	//������ �����������
	std::vector<Collaborator*> _vecSubordinates;
	// ��� ����������
	std::string _nameCollaborator;
	//��������
	double _salary;
	//������� ������
	double _baseRate;
	//���� ������ �� ������
	std::string _dayOfHiring;
	
	//������� ���������� ���, ������� ��������
	int CountYearsOfWorkign(std::string date);
	short _yearsOfWorking;


};
