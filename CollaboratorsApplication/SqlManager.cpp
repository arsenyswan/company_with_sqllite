#include "SqlManager.h"
#include "Global.h"

SqlManager::SqlManager()
{
	_db=0;
	_zErrMsg = 0;
	//SqlManager("test.db");
}
SqlManager::SqlManager(std::string nameOfDB)
{
	_db=0;
	_zErrMsg = 0;
	sqlOpenDatabase(nameOfDB);
}

SqlManager::~SqlManager()
{
	/*if (zErrMsg)
	{
		delete zErrMsg;
	}*/
	if (_db)
	{
		sqlite3_close(_db);
	}
}
bool SqlManager::sqlDbIsOpen()
{
	return _db!=0;
}
static int callbackPrint(void *data, int argc, char **argv, char **azColName) {
	for (int i = 0; i<argc; i++) 
	{
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}
int SqlManager::sqlPrepareExec(std::string sqlCommand)
{
	_rc = sqlite3_prepare_v2( _db, sqlCommand.c_str(), -1, &_stmt, 0);//-1 = sqlCommand.size()
	if (_rc != SQLITE_OK) 
	{
        fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(_db));
        sqlite3_close(_db);
        return SQLITE_ERROR;
    }    
	sqlite3_step(_stmt); 
	sqlite3_reset(_stmt);
	sqlite3_finalize(_stmt); 
	return _rc;
}


int SqlManager::sqlStepExec(std::string sqlCommand,std::vector<Collaborator*> &vecCol)
{
	_rc = sqlite3_prepare_v2( _db, sqlCommand.c_str(), -1, &_stmt, 0);//-1 = sqlCommand.size()
	if (_rc != SQLITE_OK) 
	{   
        fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(_db));
        sqlite3_close(_db);
        return SQLITE_ERROR;
    }    
	while (sqlite3_step(_stmt) == SQLITE_ROW )
	{
		//char **columns;
		std::string columns[7];// = new std::string[sqlite3_column_count(_stmt)];
		for (int i = 0; i < sqlite3_column_count(_stmt); i++)
		{
			columns[i] = (char*)sqlite3_column_text(_stmt,i) != NULL?(char*)sqlite3_column_text(_stmt,i):std::to_string(_WITHOUTBOSS);//"-1"
		}
		addCollaboratorToVector(vecCol,columns);
	}
	sqlite3_reset(_stmt);
	sqlite3_finalize(_stmt); 
	return SQLITE_OK;
}


int SqlManager::sqlExec(std::string sqlCommand)
{
	return sqlExec(sqlCommand, callbackPrint, nullptr);
}
int SqlManager::sqlExec(std::string sqlCommand, int(*specialCallback)(void*, int, char**, char**), void *specialData)
{
	_rc = sqlite3_exec(_db, sqlCommand.data(), specialCallback, (void*)specialData, &_zErrMsg);
	if (_rc != SQLITE_OK){
		fprintf(stderr, "SQL error: %s\n", _zErrMsg);
		sqlite3_free(_zErrMsg);
		return SQLITE_ERROR;
	}
	else {
		fprintf(stdout, "Command executed successfully\n");
		return SQLITE_OK;
	}
}
int SqlManager::sqlOpenDatabase(std::string dbName)
{
	/* Open database */
	_rc = sqlite3_open(dbName.data(), &_db);

	if (_rc) {
		fprintf(stderr, "Can't open database %s: %s\n", dbName.data(), sqlite3_errmsg(_db));
		return(SQLITE_ERROR);//throw (SQLITE_ERROR);
	}
	else {
		//fprintf(stdout, "Opened database successfully\n");
	}
	return(SQLITE_OK);
}