#pragma once
#ifndef GLOBAL_H
#define GLOBAL_H

#include "SqlManager.h"
#include "Collaborator.h"
#include "Employee.h"
#include "Manager.h"
#include "Sales.h"

int menu();
int openOrCreateDatabase(SqlManager& sqlObj, std::vector<Collaborator*> &vecCol);
int addAnEntryToDatabase( SqlManager& sqlObj, std::vector<Collaborator*> &vecCol);
int updateRecordInDatabase( SqlManager& sqlObj, std::vector<Collaborator*> &vecCol);
int ˝alculateSalaryForSelectedDate( SqlManager& sqlObj, std::vector<Collaborator*> &vecCol);
Collaborator* inputCollaborator(std::vector<Collaborator*> &vecCol);
int showDatabase( SqlManager &sqlObj,std::vector<Collaborator*> &vecCol);
int showListOfSubordinates(SqlManager &sqlObj,std::vector<Collaborator*> &vecCol);

Collaborator* recognizeTypeOfColloborator(std::string *columns);

void addCollaboratorToVector(std::vector<Collaborator*> &vecCol, std::string *columns);

void printVec(const std::vector<Collaborator*> vecCol);

//std::string recognizeTypeOfColloborator(Collaborator* col);



#endif