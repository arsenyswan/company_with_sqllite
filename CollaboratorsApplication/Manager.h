#pragma once
#include "Collaborator.h"
class Manager :
	public Collaborator
{
public:
	Manager();
	~Manager();
	double CalculateSalary(std::string date);
	std::string getTypeName();
};

