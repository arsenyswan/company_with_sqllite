#pragma once
#ifndef SQL_MANAGER
#define SQL_MANAGER

#include <string> 
#include "sqlite3.h"
#include <vector>
#include "Collaborator.h"

class SqlManager
{
public:
	SqlManager();
	SqlManager(std::string nameOfDB);
	~SqlManager();
	int sqlExec(std::string sqlRequest);
	int sqlOpenDatabase(std::string dbName);
	int sqlExec(std::string sqlCommand, int(*callback)(void*, int, char**, char**), void *data);
	bool sqlDbIsOpen();

	int sqlPrepareExec(std::string sqlCommand);
	int sqlStepExec(std::string sqlCommand, std::vector<Collaborator*> &vecCol);
private:
	std::string _strSqlRequest;
	
	sqlite3 *_db;
	char *_zErrMsg;
	int _rc;
public: sqlite3_stmt *_stmt;
	
};
#endif