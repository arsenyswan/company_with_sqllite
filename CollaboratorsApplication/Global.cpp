#include "Global.h"
#include <fstream>

//������� ������� ����
int menu()
{
	SqlManager sqlObj;
	std::vector<Collaborator*> vecCol;
	char choice;
	do {
		std::cout << std::endl;
		std::cout << "-------------- Choose acttion --------------" << std::endl;
		std::cout << "1. Open or create database" << std::endl;
		std::cout << "2. Add an entry to the database" << std::endl;
		std::cout << "3. Update record in the database" << std::endl;
		std::cout << "4. Calculate salary for the selected date" << std::endl;
		std::cout << "5. Show database" << std::endl;
		std::cout << "6. Show list of subordinates" << std::endl;
		std::cout << "7. Exit" << std::endl << std::endl;
		std::cout << "Input number from 1 to 7: ";
		std::cin >> choice;

		switch (choice) {
		case '1':
			openOrCreateDatabase(sqlObj, vecCol);
			break;
		case '2':
			addAnEntryToDatabase(sqlObj, vecCol);
			break;
		case '3':
			updateRecordInDatabase(sqlObj, vecCol);
			break;
		case '4':
			�alculateSalaryForSelectedDate(sqlObj, vecCol);
			break;
		case '5':
			showDatabase(sqlObj,vecCol);
			break;
		case '6':
			showListOfSubordinates(sqlObj,vecCol);
			break;
		case '7':
			std::cout << "Goodbye.\n";
			sqlObj.~SqlManager();
			break;

		default:
			std::cout<<"Incorrect input, try again" << std::endl;
		}

	} while (choice != '7');
	return 0;
}

int showListOfSubordinates(SqlManager &sqlObj,std::vector<Collaborator*> &vecCol)
{
	if (!sqlObj.sqlDbIsOpen())
	{
		std::cout << "The database is not open, first open the database" << std::endl;
		return 1;
	}
	int IDToChange;
	std::cout << "Intput ID of collaborator whose subordinates you want to show: ";
	std::cin >> IDToChange;
	if (IDToChange<=vecCol.size())
	{
		std::cout << vecCol[IDToChange-1]->getName() << " list of subordinates:"<< std::endl;
		vecCol[IDToChange-1]->printSubordinates(std::cout);
	}
}
int �alculateSalaryForSelectedDate( SqlManager& sqlObj, std::vector<Collaborator*> &vecCol)
{ 
	if (!sqlObj.sqlDbIsOpen())
	{
		std::cout << "The database is not open, first open the database" << std::endl;
		return 1;
	}
	if (vecCol.size() == 0)
	{
		std::cout << "Database is empty" << std::endl;
		return 1;
	}

	std::string date;
	std::cout << "Select date in format YYYY-MM-DD: ";
	std::cin >> date;
	
	std::cout << "_____________Chose one______________"<<std::endl; 
	std::cout << "1. Calculate summary salary" <<std::endl;
	std::cout << "2. Calculate salary for person by ID" <<std::endl;

	char choice;
	std::cin >>choice;
	if (choice == '2')
	{
		int ID;
		std::cout << "Select the collaborator for whom you want to calculate the salary, enter his ID: ";
		std::cin >> ID;

		if(ID <= vecCol.size())
		{			
			vecCol[ID-1]->CalculateSalary(date);
			std:: cout << vecCol[ID-1]->getName() << " will receive " << vecCol[ID-1]->getSalary();
		}
	}
	else 
	{
		double summary = 0;
		for each (Collaborator* col in vecCol)
		{
			col->CalculateSalary(date);
			summary += col->getSalary();	
		}
		std::cout << "Summary salary = " << summary;
	}
}

//����������� ��������� ��������� �� ������ �� ����
Collaborator* recognizeTypeOfColloborator(std::string *columns)
{
	Collaborator *col = nullptr;
	if(columns[1] == _MANAGER)
		col = new Manager();
	if(columns[1] == _SALES)
		col = new Sales();
	if(columns[1] == _EMPLOYEE)
		col = new Employee();
	if(col == nullptr) 
		throw "Can't recognozed type of object. Column 1 in db has bad value";
	return col;
}

//���������� ������ � ��������� �� ���� ������
void addCollaboratorToVector(std::vector<Collaborator*> &vecCol, std::string *columns)
{
	Collaborator *col = recognizeTypeOfColloborator(columns);
	col->setID(columns[0]);
	col->setNameCollaborator(columns[2]);
	col->setDayOfHiring(columns[3]);
	col->setBaseRate(columns[4]);
	col->setBossID(columns[6]);
	vecCol.push_back(col);
}

//����� ����������� ������� ����������
void printVec(std::vector<Collaborator*> vecCol)
{
	for each (Collaborator* col in vecCol)
	{
		col->CalculateSalary("");
		col->print();
		std::cout << std::endl;
	}

}
//�������� ����� ���� ������, ��� �������� � ������ ������ �� ������������ ����
int openOrCreateDatabase(SqlManager& sqlObj, std::vector<Collaborator*> &vecCol)
{
	vecCol.clear();
	std::string dbName;
	std::cout << "Input name of database without extention: ";
	std::cin >> dbName;
	dbName+=".db";
	if (!std::ifstream(dbName))//���� ���� ������ ��� - ������� ���� ������
	{
		sqlObj.sqlOpenDatabase(dbName);
		if (sqlObj.sqlPrepareExec(Collaborator::SqlCommandToCreateTableInDB())==SQLITE_OK)
		{
			std::cout << "Database " << dbName << " created successfully." << std::endl;
		}
	}
	else //���� ���� ������ ���� - ������� ���� � ��������� �� ��� ������
	{
		sqlObj.sqlOpenDatabase(dbName);
		if(sqlObj.sqlStepExec("SELECT * FROM COLLABORATOR", vecCol)==SQLITE_OK)
		{
			Collaborator::fillSubordinatesList(vecCol);
			std::cout << "Database " << dbName << " was opened and loaded successfully." << std::endl;
		}
	}
	return 1;
}

//����� ����������� ���� ������, ���� �� ����� ������� ������� ������ � ���� ���� ������
int showDatabase( SqlManager &sqlObj,std::vector<Collaborator*> &vecCol)
{
	if (!sqlObj.sqlDbIsOpen())
	{
		std::cout << "The database is not open, first open the database" << std::endl;
		return 1;
	}
	if (vecCol.size() == 0)
	{
		std::cout << "Database is empty" << std::endl;
		return 1;
	}
	printVec(vecCol);
	return 0;
}



int updateRecordInDatabase( SqlManager& sqlObj, std::vector<Collaborator*> &vecCol)
{
	if (!sqlObj.sqlDbIsOpen())
	{
		std::cout << "The database is not open, first open the database" << std::endl;
		return 1;
	}
	int IDToChange;
	std::cout << "Intput ID of collaborator to change: ";
	std::cin >> IDToChange;
	if (IDToChange<=vecCol.size())
	{
		Collaborator* col = inputCollaborator(vecCol);
		col->setID(std::to_string(IDToChange));
		vecCol[IDToChange-1] = col;
		sqlObj.sqlPrepareExec(Collaborator::sqlCommandToUpdateDataInTable("COLLABORATOR", col, IDToChange));
	}
	else 
	{
		std::cout << "Collaborator with " << IDToChange << " ID not found" <<std::endl;
	}
	Collaborator::fillSubordinatesList(vecCol);
}
Collaborator* inputCollaborator(std::vector<Collaborator*> &vecCol)
{
	Collaborator *col;
	char choice;
	do
	{
		std::cout << "Input post of collaborator:" << std::endl;
		std::cout << "1: " << _MANAGER << std::endl;
		std::cout << "2: " << _SALES << std::endl;
		std::cout << "3: " << _EMPLOYEE << std::endl;
		std::cout << "Possible input (1,2,3): ";
		std::cin >> choice;
	}while (choice!='1' && choice!='2' && choice!='3');

	switch (choice)
	{
	case '1':
		col = new Manager();
		break;
	case '2':
		col = new Sales();
		break;
	case '3':
		col = new Employee();
		break;
	default:
		throw "Impossible post of collaborator";
		break;
	}
	col->setID(std::to_string(vecCol.size()+1));

	//�� ��� �������� �� ������������ ������� ������
	std::string getterString;	
	std::cout << "Input name of collaborator: ";
	getchar();
	std::getline(std::cin,getterString);
	col->setNameCollaborator(getterString);

	std::cout << "Input base rate for " << col->getName() << ": ";
	std::cin >> getterString;
	col->setBaseRate(getterString);

	std::cout << "Input day of hiring "<< col->getName() << " in format YYYY-MM-DD: ";
	std::cin >> getterString;
	col->setDayOfHiring(getterString);


	if( vecCol.size()>0)
	{
		std::cout << "Does " << col->getName() << " have a boss(y/n): ";
		std::cin >> choice;
		std::cout << std::endl;
		if (choice=='y')
		{
			//����������� ID ����� ���� �� ����� ������� ������ ����� � id ������ ��� ���������� ���� ���������� � ���� 
			do
			{
				std::cout << "Input " << col->getName() << "'s boss id: ";
				std::cin >> getterString;
			} while(!([](const std::string& getterString) 
			{return getterString.find_first_not_of("0123456789") == std::string::npos;}(getterString)) &&
			atoi(getterString.c_str())>vecCol.size()
			);
			if (vecCol[atoi(getterString.c_str())-1]->getTypeName() == _EMPLOYEE)
			{
				std::cout << "Employee can't be the boss" << std::endl;
			}
			else 
			{
				col->setBossID(getterString);
			}
		}
	}
	return col;
}

//�������� ������ � ���� ������
int addAnEntryToDatabase(SqlManager& sqlObj, std::vector<Collaborator*> &vecCol)
{
	if (!sqlObj.sqlDbIsOpen())
	{
		std::cout << "The database is not open, first open the database" << std::endl;
		return 1;
	}
	Collaborator* col = inputCollaborator(vecCol);

	vecCol.push_back(col);
	col->setID(std::to_string(vecCol.size()));
	sqlObj.sqlPrepareExec(Collaborator::sqlCommandToInsertDataInTable("COLLABORATOR",col));
	Collaborator::fillSubordinatesList(vecCol);
}