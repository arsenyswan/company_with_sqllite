#pragma once
#include "Collaborator.h"
class Employee :
	public Collaborator
{
public:
	double CalculateSalary(std::string date);
	Employee();
	~Employee();
	std::string getTypeName();
};

