#include "Collaborator.h"
#include "Global.h"

Collaborator::Collaborator()
{
	_bossID=-1;
}


//������� ���������� ��������� ������ ��� ��� �������� ����
int  Collaborator::CountYearsOfWorkign(std::string date)
{
	
	if (date =="")
	{
		time_t t = time(0);   // get time now
		struct tm * now = new tm;
		localtime_s(now, &t);
		char buf[20];
		strftime(buf, sizeof(buf), "%Y-%m-%d", now);//"%Y-%m-%d.%X"
		date = buf;	
	}
	
    int numOfYears=0;
	try
	{
		numOfYears = atoi(date.substr(0, 4).c_str())- atoi(_dayOfHiring.substr(0, 4).c_str());
		if (atoi(date.substr(5, 2).c_str()) - atoi(_dayOfHiring.substr(5, 2).c_str()) < 0)
		{
			numOfYears--;
		}
		if ((date.substr(5, 2)) == _dayOfHiring.substr(5, 2) &&
			atoi(date.substr(8).c_str()) - atoi(_dayOfHiring.substr(8).c_str()) < 0) 
		{
			numOfYears--;
		}
	}
	catch (...)
	{
		std::cout << "WARNING!!! Date of hiring is incorrect! Bad calculation of years" <<std::endl;
	}
	return numOfYears>0?numOfYears:0;
}
Collaborator::~Collaborator()
{
}

std::string Collaborator::SqlCommandToCreateTableInDB(std::string nameOfTable)
{
	return "CREATE TABLE " + nameOfTable + "(" \
		"ID                 INTEGER PRIMARY KEY     AUTOINCREMENT," \
		"TYPE_COLLABORATOR  TEXT    NOT NULL,"\
		"NAME               TEXT    NOT NULL," \
		"DAY_OF_HIRING      TEXT    NOT NULL," \
		"BASE_RATE          REAL    NOT NULL," \
		"SALARY             REAL,"\
		"BOSS_ID            INTEGER);";
}

std::string Collaborator::SqlCommandToCreateTableInDB()
{
	return SqlCommandToCreateTableInDB("COLLABORATOR");
}


std::string Collaborator::sqlCommandToInsertDataInTable(std::string nameOfTable, std::string type, std::string name, std::string dayOfHiring, std::string baseRate, std::string bossID)
{
	return "INSERT INTO "+nameOfTable + 
		"(TYPE_COLLABORATOR,NAME,DAY_OF_HIRING,BASE_RATE,BOSS_ID) "  \
		"VALUES ('" + type + "', '" 
					+ name + "', '" 
		            + dayOfHiring + "', "
					+ baseRate + ", "
		            + bossID +" );";
}
std::string Collaborator::sqlCommandToInsertDataInTable(std::string nameOfTable, std::string type, std::string name, std::string dayOfHiring, std::string baseRate)
{
	return "INSERT INTO "+nameOfTable + 
		"(TYPE_COLLABORATOR,NAME,DAY_OF_HIRING,BASE_RATE) "  \
		"VALUES ('" + type + "', '" 
					+ name + "', '" 
		            + dayOfHiring + "', "
		            + baseRate +" );";
}
std::string Collaborator::sqlCommandToInsertDataInTable(std::string nameOfTable, Collaborator *col)
{
	if(col->getBossID()==_WITHOUTBOSS)
	{
		return sqlCommandToInsertDataInTable(nameOfTable, col->getTypeName(), col->getName(), col->getDayOfHiring(), col->getBaseRate());
	}
	return sqlCommandToInsertDataInTable(nameOfTable, col->getTypeName() ,col->getName(), col->getDayOfHiring(), col->getBaseRate(), std::to_string(col->getBossID()));
}
std::string Collaborator::sqlCommandToUpdateDataInTable(std::string nameOfTable, Collaborator *col, int ID)
{
	return "UPDATE "                  + nameOfTable +						
		   " SET NAME = '"            + col->getName() +
		   "', TYPE_COLLABORATOR = '" + col->getTypeName() +
		   "', DAY_OF_HIRING = '"     + col->getDayOfHiring() +				
		   "', BASE_RATE = "          + col->getBaseRate() +			
		   ", BOSS_ID = "             + std::to_string(col->getBossID()) +	
		   " WHERE ID = "             +std::to_string(ID) +";";				
}


std::string Collaborator::getDayOfHiring(){return _dayOfHiring;}
std::string Collaborator::getName(){return _nameCollaborator;}
std::string Collaborator::getBaseRate(){return std::to_string(_baseRate);}
double Collaborator::getSalary(){return _salary;};

void Collaborator::printSubordinates(std::ostream &of)
{
	for each (Collaborator* col in this->_vecSubordinates)
	{
		of << col->getName() << std::endl;
	}
}
void Collaborator::print(std::ostream &of)
{
	of << "ID: " << _ID << std::endl;
	of << "Post: " << getTypeName().substr(0) << std::endl;;
	of << "Name: " <<_nameCollaborator <<std::endl;
	of << "Day of hiring: " << _dayOfHiring << std::endl;
	of << "Base rate: " << _baseRate << std::endl;
	of << "Salary: " << _salary << std::endl;
	if(_bossID!=_WITHOUTBOSS)
	{
		of << "Boss ID: "<<_bossID << std::endl;
		of << "Boss Name: "<<_boss->getName() << std::endl;
	}
}
void Collaborator::fillSubordinatesList(std::vector<Collaborator*> & vec)
{
	for each (Collaborator* col in vec)
	{
		col->_vecSubordinates.clear();
		col->_boss = nullptr;
	}
	for each (Collaborator* col in vec)
	{
		if(col->getBossID()!=_WITHOUTBOSS)
		{
			col->setBoss(vec[col->getBossID()-1]);
			vec[col->getBossID()-1]->addSubordinate(col);
		}
	}
}
void Collaborator::addSubordinate(Collaborator* subordinate)
{
	this->_vecSubordinates.push_back(subordinate);
}
void Collaborator::setBoss(Collaborator* boss)
{
	this->_boss = boss;
}
int Collaborator::getBossID()
{
	return _bossID;
}
void Collaborator::print()
{
	print(std::cout);
}
