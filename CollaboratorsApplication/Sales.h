#pragma once
#include "Collaborator.h"
class Sales :
	public Collaborator
{
public:

	Sales();
	~Sales();
	double Sales::CalculateSalary(std::string date);
	std::string getTypeName();
	
};

